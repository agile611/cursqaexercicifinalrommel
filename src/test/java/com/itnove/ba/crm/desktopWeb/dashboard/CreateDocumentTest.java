package com.itnove.ba.crm.desktopWeb.dashboard;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.*;
import org.testng.annotations.Test;

import java.io.File;
import java.util.UUID;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CreateDocumentTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        String name = UUID.randomUUID().toString();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.createButtonClick(hover);
        dashboardPage.clickOnCreateDocumentLink(hover);
        CreateDocumentPage createDocumentPage = new CreateDocumentPage(driver);
        File file = new File("src" + File.separator + "main" + File.separator + "resources" + File.separator + "2-logo-B_activa.png");
        createDocumentPage.browseFile(file.getAbsolutePath());
        createDocumentPage.saveDocument();
     }
}
