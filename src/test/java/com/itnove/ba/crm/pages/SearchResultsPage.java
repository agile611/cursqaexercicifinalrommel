package com.itnove.ba.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class SearchResultsPage {

    private WebDriver driver;

    @FindBy(xpath = ".//*[@class='search_form']")
    public WebElement searchForm;

    @FindBy(id = "searchFieldMain")
    public WebElement searchFieldMain;

    @FindBy(xpath = ".//*[@id='pagecontent']/table/tbody[2]/tr/td[2]/a")
    public WebElement results;

    @FindBy(xpath = ".//*[@id='pagecontent']/table/tbody[2]/tr/td")
    public WebElement noResults;

    public boolean isSearchResultsPageLoaded(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(searchForm));
        return searchForm.isDisplayed();
    }

    public boolean isNoSearchResultsDisplayed(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(noResults));
        return noResults.isDisplayed();
    }

    public String isSearchKeywordCorrect(){
        return searchFieldMain.getAttribute("value");
    }
    public void clickOnFirstResult(WebDriverWait wait) throws InterruptedException {
        results.click();
    }


    public SearchResultsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
