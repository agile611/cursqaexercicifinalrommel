package com.itnove.ba.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class LoginPage {

    private WebDriver driver;

    @FindBy(id = "user_name")
    public WebElement usernameTextBox;
    @FindBy(xpath = ".//*[@id='user_name']")
    public WebElement usernameTextBoxMob;

    @FindBy(id = "user_password")
    public WebElement userpasswordTextBox;
    @FindBy(xpath = ".//*[@id='user_password']")
    public WebElement userpasswordTextBoxMob;

    @FindBy(id = "bigbutton")
    public WebElement botoLogin;
    @FindBy(xpath = ".//*[@id='bigbutton']")
    public WebElement botoLoginMob;

    @FindBy(xpath = ".//span[@class='error'][3]")
    public WebElement errorMessage;

    @FindBy(xpath = ".//span[@class='error'][3]")
    public WebElement errorMessageMob;

    @FindBy(xpath = "html/body/div[1]/div[2]/p")
    public WebElement sessionExpired;
    @FindBy(xpath = "html/body/div[1]/div[2]/p")
    public WebElement sessionExpiredMob;

    public void login(String user, String passwd){
        usernameTextBox.clear();
        usernameTextBox.sendKeys(user);
        userpasswordTextBox.clear();
        userpasswordTextBox.sendKeys(passwd);
        botoLogin.click();
    }
    public void loginMob(RemoteWebDriver driver1, String user, String passwd){
        usernameTextBoxMob.clear();
        usernameTextBoxMob.sendKeys(user);
        userpasswordTextBoxMob.clear();
        userpasswordTextBoxMob.sendKeys(passwd);
        botoLogin.click();
    }

    public boolean isErrorMessagePresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(errorMessage));
        return errorMessage.isDisplayed();
    }

    public boolean isLoginButtonPresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(botoLogin));
        return botoLogin.isDisplayed();
    }

    public String errorMessageDisplayed(){
        return errorMessage.getText();
    }

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
