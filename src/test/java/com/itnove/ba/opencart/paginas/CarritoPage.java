package com.itnove.ba.opencart.paginas;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by rommel http://opencart.votarem.lu.
 */
public class CarritoPage {

    private WebDriver driver;

    @FindBy(xpath = ".//*[@id='top-links']/ul/li[4]/a/i")
    public  WebElement botonCarrito;

    @FindBy(xpath = ".//*[@id='content']/form/div/table[1]/tbody/tr")
    public List<WebElement> listaElementos;

    public boolean existeElemntoLista(String elemento){
        Boolean respuesta= false;

        for (WebElement elemLista :listaElementos) {

            respuesta=elemLista.getText().contains(elemento);

        }
        return respuesta;
    }

/*
    public void clickOnBotonBusqueda(Actions hover) throws InterruptedException {
        botonBusqueda.click();
    }

    public boolean listaBusquedaConElementos(WebDriverWait wait) {
        if (listaBusqueda.size() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public boolean listaBusquedaSinElementos(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(mensajelistaVacia));
        return mensajelistaVacia.isDisplayed();
    }

    public boolean textBusquedaIsPresent(WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(resultadoBusqueda));
        return resultadoBusqueda.isDisplayed();
    }
    public boolean mensajeSeAnyadeElemntoExitoIsPrensent(WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(mensajeSeAnyadeElemntoExito));
        return mensajeSeAnyadeElemntoExito.isDisplayed();
    }*/

    /*
    public void login(String user, String passwd) throws InterruptedException {

        usernameTextBox.clear();
        usernameTextBox.sendKeys(user);
        userpasswordTextBox.clear();
        userpasswordTextBox.sendKeys(passwd);
        botonLogin.click();
    }
    public void clickOnBotonBUsqueda(Actions hover) throws InterruptedException {
        iconoMyAccount.click();

    }
    public void clickOnLogin(Actions hover) throws InterruptedException {
        iconoMyAccount.click();
        opcionLogin.click();

    }
    public void clickOnRegister(Actions hover) throws InterruptedException {
        iconoMyAccount.click();
        opcionRegistro.click();

    }*/
/*
    public boolean isErrorMessagePresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(errorMessage));
        return errorMessage.isDisplayed();
    }

    public boolean isLoginButtonPresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(botoLogin));
        return botoLogin.isDisplayed();
    }

    public String errorMessageDisplayed(){
        return errorMessage.getText();
    }
*/
    public CarritoPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
