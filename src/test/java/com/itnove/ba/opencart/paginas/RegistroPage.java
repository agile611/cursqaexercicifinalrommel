package com.itnove.ba.opencart.paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

/**
 * Created by guillem on 01/03/16.
 */
public class RegistroPage {

    private WebDriver driver;

    @FindBy(xpath = ".//*[@id='input-email']")
    public WebElement usernameTextBox;

    @FindBy(xpath = ".//*[@id='input-password']")
    public WebElement userpasswordTextBox;

    @FindBy(xpath = ".//*[@id='top-links']/ul/li[2]")
    public WebElement iconoMyAccount;

    @FindBy(xpath = ".//*[@id='top-links']/ul/li[2]/ul/li[2]")
    public WebElement opcionLogin;

    @FindBy(xpath = ".//*[@id='top-links']/ul/li[2]/ul/li[1]")
    public WebElement opcionRegistro;

    @FindBy(xpath = ".//*[@id='content']/div/div[2]/div/form/input")
    public WebElement botonLogin;

    @FindBy(xpath = ".//*[@id='input-firstname']")
    public WebElement firstname;
    @FindBy(xpath = ".//*[@id='input-firstname']")
    public WebElement firstnameMob;

    @FindBy(xpath = ".//*[@id='input-lastname']")
    public WebElement lastName;
    @FindBy(xpath = ".//*[@id='input-lastname']")
    public WebElement lastNameMob;

    @FindBy(xpath = ".//*[@id='input-email']")
    public WebElement email;
    @FindBy(xpath = ".//*[@id='input-email']")
    public WebElement emailMob;

    @FindBy(xpath = ".//*[@id='input-telephone']")
    public WebElement telefono;
    @FindBy(xpath = ".//*[@id='input-telephone']")
    public WebElement telefonoMob;

    @FindBy(xpath = ".//*[@id='input-password']")
    public WebElement password;
    @FindBy(xpath = ".//*[@id='input-password']")
    public WebElement passwordMob;

    @FindBy(xpath = ".//*[@id='input-confirm']")
    public WebElement confimPassword;
    @FindBy(xpath = ".//*[@id='input-confirm']")
    public WebElement confimPasswordMob;

    @FindBy(xpath = ".//*[@id='content']/form/div/div/input[1]")
    public WebElement politicaPrivacidad;
    @FindBy(xpath = ".//*[@id='content']/form/div/div/input[1]")
    public WebElement politicaPrivacidadMob;

    @FindBy(xpath = ".//*[@id='content']/form/div/div/input[2]")
    public WebElement botonContinue;
    @FindBy(xpath = ".//*[@id='content']/form/div/div/input[2]")
    public WebElement botonContinueMob;

    @FindBy(xpath = ".//*[@id='account-register']/div[1]")
    public WebElement errorMensajePoliticaPrivacidad;
    @FindBy(xpath = ".//*[@id='account-register']/div[1]")
    public WebElement errorMensajePoliticaPrivacidadMob;

    @FindBy(xpath = ".//*[@id='account']/div[2]/div/div")
    public WebElement errorMensajeFirstName;
    @FindBy(xpath = ".//*[@id='account']/div[2]/div/div")
    public WebElement errorMensajeFirstNameMob;

    @FindBy(xpath = ".//*[@id='account']/div[3]/div/div")
    public WebElement errorMensajeLastName;
    @FindBy(xpath = ".//*[@id='account']/div[3]/div/div")
    public WebElement errorMensajeLastNameMob;

    @FindBy(xpath = ".//*[@id='account']/div[4]/div/div")
    public WebElement errorMensajeTelefono;
    @FindBy(xpath = ".//*[@id='account']/div[4]/div/div")
    public WebElement errorMensajeTelefonoMob;

    @FindBy(xpath = ".//*[@id='content']/form/fieldset[2]/div[1]/div/div")
    public WebElement errorMensajePass;
    @FindBy(xpath = ".//*[@id='content']/form/fieldset[2]/div[1]/div/div")
    public WebElement errorMensajePassMob;

    @FindBy(xpath = ".//*[@id='content']/form/fieldset[2]/div[2]/div/div")
    public WebElement errorMensajeConfirmPass;
    @FindBy(xpath = ".//*[@id='content']/form/fieldset[2]/div[2]/div/div")
    public WebElement errorMensajeConfirmPassMob;

    @FindBy(xpath = "..//*[@id='content']/p[1]")
    public WebElement exitoMensaje;
    @FindBy(xpath = "..//*[@id='content']/p[1]")
    public WebElement exitoMensajeMob;

    public void clickOnRegister(Actions hover) throws InterruptedException {
        hover.moveToElement(iconoMyAccount).moveToElement(iconoMyAccount);
        Thread.sleep(3000);
        iconoMyAccount.click();
        opcionRegistro.click();
    }

    public boolean noMuestraErrorMensajeFirstName (WebDriver driver, WebDriverWait wait){
        return !errorMensajeFirstName.isDisplayed();

    }
    public boolean noMuestraErrorMensajeLastName(WebDriver driver, WebDriverWait wait){
        return !errorMensajeLastName.isDisplayed();
    }
    public boolean noMuestraErrorMensajeTelefono(WebDriver driver, WebDriverWait wait){
        return !errorMensajeTelefono.isDisplayed();
    }
    public boolean noMuestraErrorMensajePass(WebDriver driver, WebDriverWait wait){
        return !errorMensajePass.isDisplayed();
    }

    /*
        public boolean isLoginButtonPresent(WebDriver driver, WebDriverWait wait){
            wait.until(ExpectedConditions.visibilityOf(botoLogin));
            return botoLogin.isDisplayed();
        }

        public String errorMessageDisplayed(){
            return errorMessage.getText();
        }
    */
    public RegistroPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
