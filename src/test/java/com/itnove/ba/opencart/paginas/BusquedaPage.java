package com.itnove.ba.opencart.paginas;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by rommel http://opencart.votarem.lu.
 */
public class BusquedaPage {

    private WebDriver driver;

    @FindBy(xpath = ".//*[@id='search']/input")
    public WebElement textBoxBusqueda;
    @FindBy(xpath = ".//*[@id='search']/input")
    public WebElement textBoxBusquedaMob;
    @FindBy(xpath = ".//*[@id='search']/span/button")
    public WebElement botonBusqueda;
    @FindBy(xpath = ".//*[@id='search']/span/button")
    public WebElement botonBusquedaMob;
    @FindBy(xpath = ".//*[@id='content']/h1")
    public WebElement resultadoBusqueda;
    @FindBy(xpath = ".//*[@id='content']/h1")
    public WebElement resultadoBusquedaMob;
    @FindBy(xpath = ".//*[@id='content']/div[3]/div")
    public List<WebElement> listaBusqueda;
    @FindBy(xpath = ".//*[@id='content']/div[3]/div/div/div[1]")
    public List<WebElement> listaBusquedaMob;
    @FindBy(xpath = ".//*[@id='content']/p[2]")
    public WebElement mensajelistaVacia;
    @FindBy(xpath = ".//*[@id='product-search']/div[1]")
    public WebElement mensajeSeAnyadeElemntoExito;
    @FindBy(xpath = ".//*[@id='content']/div[3]/div[1]/div/div[2]/div[2]/button[1]")
    public  WebElement botonAnyadirAlCarroPrimerElemento;
    @FindBy(xpath = ".//*[@id='content']/div[3]/div[1]/div/div[2]/div[2]/button[1]")
    public  WebElement botonAnyadirAlCarroPrimerElementoMob;
    @FindBy(xpath = ".//*[@id='top-links']/ul/li[4]/a/i")
    public  WebElement botonCarrito;


    public void clickOnBotonBusqueda(Actions hover) throws InterruptedException {
        botonBusqueda.click();
    }

    public boolean listaBusquedaConElementos(WebDriverWait wait) {
        if (listaBusqueda.size() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public boolean listaBusquedaSinElementos(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(mensajelistaVacia));
        return mensajelistaVacia.isDisplayed();
    }

    public boolean textBusquedaIsPresent(WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(resultadoBusqueda));
        return resultadoBusqueda.isDisplayed();
    }
    public boolean textBusquedaIsPresent(RemoteWebDriver driver1,WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(resultadoBusqueda));
        return resultadoBusqueda.isDisplayed();
    }
    public boolean textBusquedaIsPresentMob(WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(resultadoBusqueda));
        return resultadoBusquedaMob.isDisplayed();
    }
    public boolean mensajeSeAnyadeElemntoExitoIsPrensent(WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(mensajeSeAnyadeElemntoExito));
        return mensajeSeAnyadeElemntoExito.isDisplayed();
    }
    public boolean mensajeSeAnyadeElemntoExitoIsPrensent(RemoteWebDriver driver1,WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(mensajeSeAnyadeElemntoExito));
        return mensajeSeAnyadeElemntoExito.isDisplayed();
    }

    /*
    public void login(String user, String passwd) throws InterruptedException {

        usernameTextBox.clear();
        usernameTextBox.sendKeys(user);
        userpasswordTextBox.clear();
        userpasswordTextBox.sendKeys(passwd);
        botonLogin.click();
    }
    public void clickOnBotonBUsqueda(Actions hover) throws InterruptedException {
        iconoMyAccount.click();

    }
    public void clickOnLogin(Actions hover) throws InterruptedException {
        iconoMyAccount.click();
        opcionLogin.click();

    }
    public void clickOnRegister(Actions hover) throws InterruptedException {
        iconoMyAccount.click();
        opcionRegistro.click();

    }*/
/*
    public boolean isErrorMessagePresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(errorMessage));
        return errorMessage.isDisplayed();
    }

    public boolean isLoginButtonPresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(botoLogin));
        return botoLogin.isDisplayed();
    }

    public String errorMessageDisplayed(){
        return errorMessage.getText();
    }
*/
    public BusquedaPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
