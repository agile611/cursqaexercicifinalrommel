package com.itnove.ba.opencart.paginas;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class LoginPage {

    private WebDriver driver;

    @FindBy(xpath = ".//*[@id='input-email']")
    public WebElement usernameTextBox;
    @FindBy(xpath = ".//*[@id='input-email']")
    public WebElement usernameTextBoxMob;

    @FindBy(xpath = ".//*[@id='input-password']")
    public WebElement userpasswordTextBox;
    @FindBy(xpath = ".//*[@id='input-password']")
    public WebElement userpasswordTextBoxMob;

    @FindBy(xpath = ".//*[@id='top-links']/ul/li[2]")
    public WebElement iconoMyAccount;
    @FindBy(xpath = ".//*[@id='top-links']/ul/li[2]/a/i")
    public WebElement iconoMyAccountMob;
    @FindBy(xpath = ".//*[@id='top-links']/ul/li[2]/ul/li[2]")
    public WebElement opcionLogin;
    @FindBy(xpath = ".//*[@id='top-links']/ul/li[2]/ul/li[2]/a")
    public WebElement opcionLoginMob;
    @FindBy(xpath = ".//*[@id='top-links']/ul/li[2]/ul/li[1]")
    public WebElement opcionRegistro;
    @FindBy(xpath = ".//*[@id='top-links']/ul/li[2]/ul/li[1]/a")
    public WebElement opcionRegistroMob;
    @FindBy(xpath = ".//*[@id='content']/div/div[2]/div/form/input")
    public WebElement botonLogin;
    @FindBy(xpath = ".//*[@id='content']/div/div[2]/div/form/input")
    public WebElement botonLoginMob;

    @FindBy(xpath = ".//*[@id='top-links']/ul/li[5]/a/i")
    public  WebElement botonCheckout;

    public void login(String user, String passwd) throws InterruptedException {

        usernameTextBox.clear();
        usernameTextBox.sendKeys(user);
        userpasswordTextBox.clear();
        userpasswordTextBox.sendKeys(passwd);
        botonLogin.click();
    }
    public void loginMob(RemoteWebDriver driver1, String user, String passwd) throws InterruptedException {

        usernameTextBoxMob.sendKeys(user);
        userpasswordTextBoxMob.clear();
        userpasswordTextBoxMob.sendKeys(passwd);
        botonLoginMob.click();
    }
    public void clickOnMyAccount(Actions hover) throws InterruptedException {
        iconoMyAccount.click();

    }
    public void clickOnLogin(Actions hover) throws InterruptedException {
        iconoMyAccount.click();
        opcionLogin.click();

    }
    public void clickOnLoginMob(Actions hover) throws InterruptedException {
        iconoMyAccountMob.click();
        opcionLoginMob.click();

    }
    public void clickOnRegister(Actions hover) throws InterruptedException {
        iconoMyAccount.click();
        opcionRegistro.click();

    }
    public void clickOnRegisterMob() throws InterruptedException {
        iconoMyAccountMob.click();
        opcionRegistroMob.click();

    }

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
