package com.itnove.ba.opencart.mobileWeb.testSuiteRegistroCliente;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.paginas.LoginPage;
import com.itnove.ba.opencart.paginas.RegistroPage;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

public class RegistroSinDatosTestMob extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {

        LoginPage loginPage = new LoginPage(driver);
        RegistroPage registroPage = new RegistroPage(driver);
        loginPage.clickOnRegisterMob();
        wait.until(ExpectedConditions.visibilityOf(registroPage.firstnameMob));
        registroPage.firstnameMob.clear();
        registroPage.lastNameMob.clear();
        registroPage.emailMob.clear();
        registroPage.telefonoMob.clear();
        registroPage.passwordMob.clear();
        registroPage.confimPasswordMob.clear();

        registroPage.botonContinueMob.click();
        registroPage.errorMensajePoliticaPrivacidadMob.isDisplayed();
        registroPage.errorMensajeFirstNameMob.isDisplayed();
        registroPage.errorMensajeLastNameMob.isDisplayed();

        registroPage.errorMensajeTelefonoMob.isDisplayed();
        registroPage.errorMensajePassMob.isDisplayed();

    }
}
