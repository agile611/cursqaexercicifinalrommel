package com.itnove.ba.opencart.mobileWeb.testSuiteBuscarProductos;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.paginas.BusquedaPage;
import org.testng.annotations.Test;

public class BuscarProductosTestMob extends BaseSauceBrowserTest {

    @Test
    public void testBusquedaConElementos() throws InterruptedException {

        BusquedaPage busquedaPage = new BusquedaPage(driver);
        busquedaPage.textBoxBusquedaMob.clear();
        busquedaPage.textBoxBusquedaMob.sendKeys("iphone");
        busquedaPage.botonBusquedaMob.click();
        busquedaPage.textBusquedaIsPresentMob(wait);
        busquedaPage.mensajeSeAnyadeElemntoExitoIsPrensent(wait);

    }
    @Test
    public void testBusquedasinElementos() throws InterruptedException {

    }

}
