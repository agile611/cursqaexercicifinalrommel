package com.itnove.ba.opencart.mobileWeb.testSuiteAnyadirProdCarrito;


import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.opencart.paginas.CarritoPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class ComporbarProductoEnCarritoTestMob extends BaseSauceBrowserTest {

    String elementoEnStock = "iPhone";

    @Test
    public void testExisteProductosCarro() throws InterruptedException {


        AnyadirProductosTestMob anyadirProductosTestWeb= new AnyadirProductosTestMob();
        CarritoPage carrito = new CarritoPage(driver);

        anyadirProductosTestWeb.testAnyadirProductosCarro(driver,wait,elementoEnStock);
        carrito.botonCarrito.click();
        assertTrue(carrito.existeElemntoLista(elementoEnStock));

    }


}
