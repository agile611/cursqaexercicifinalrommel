package com.itnove.ba.opencart.mobileWeb.testSuiteRegistroCliente;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.paginas.LoginPage;
import com.itnove.ba.opencart.paginas.RegistroPage;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

public class RegistroCorrectoClienteTestMob extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {

        LoginPage loginPage = new LoginPage(driver);
        RegistroPage registroPage = new RegistroPage(driver);
        loginPage.clickOnRegisterMob();
        wait.until(ExpectedConditions.visibilityOf(registroPage.firstnameMob));
        registroPage.firstnameMob.sendKeys("lolo");
        registroPage.lastNameMob.sendKeys("lolo");
        registroPage.emailMob.sendKeys("lolo@lololo.com");
        registroPage.telefonoMob.sendKeys("12345678");
        registroPage.passwordMob.sendKeys("lolo");
        registroPage.confimPasswordMob.sendKeys("lolo");
        registroPage.politicaPrivacidadMob.click();

        registroPage.botonContinueMob.click();
        wait.until(ExpectedConditions.visibilityOf(registroPage.exitoMensajeMob));
        registroPage.exitoMensajeMob.isDisplayed();
    }
}
