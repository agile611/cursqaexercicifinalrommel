package com.itnove.ba.opencart.mobileWeb.testSuiteLogin;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.paginas.BusquedaPage;
import com.itnove.ba.opencart.paginas.LoginPage;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import javax.swing.*;

public class LoginCorrectoClienteTestMob extends BaseSauceBrowserTest {

    @Test
    public void testLoginCliente() throws InterruptedException {

        LoginPage loginPage = new LoginPage(driver);
        loginPage.clickOnLoginMob(hover);
        loginPage.loginMob(driver,"lolo@lolo.com","lolo");


    }

}
