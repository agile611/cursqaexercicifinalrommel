package com.itnove.ba.opencart.mobileWeb.testSuiteAnyadirProdCarrito;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.paginas.BusquedaPage;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class AnyadirProductosTestMob extends BaseSauceBrowserTest {

    String elementoEnStock= "iphone";


    @Test
    public void testAnyadirProductosCarro() throws InterruptedException {

        BusquedaPage busquedaPage = new BusquedaPage(driver);
        busquedaPage.textBoxBusquedaMob.clear();
        busquedaPage.textBoxBusquedaMob.sendKeys(elementoEnStock);
        busquedaPage.botonBusquedaMob.click();
        busquedaPage.textBusquedaIsPresent(wait);
        busquedaPage.botonAnyadirAlCarroPrimerElementoMob.click();
    }

    @Test
    public void testAnyadirProductosCarro(RemoteWebDriver driver1, WebDriverWait wait1, String producto) throws InterruptedException {

        BusquedaPage busquedaPage = new BusquedaPage(driver1);
        busquedaPage.textBoxBusquedaMob.clear();
        busquedaPage.textBoxBusquedaMob.sendKeys(producto);
        busquedaPage.botonBusquedaMob.click();
        busquedaPage.textBusquedaIsPresent(driver1, wait1);
        busquedaPage.botonAnyadirAlCarroPrimerElementoMob.click();
    }
}
