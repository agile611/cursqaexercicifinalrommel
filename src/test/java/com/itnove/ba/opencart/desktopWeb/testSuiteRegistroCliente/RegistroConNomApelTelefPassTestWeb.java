package com.itnove.ba.opencart.desktopWeb.testSuiteRegistroCliente;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.paginas.LoginPage;
import com.itnove.ba.opencart.paginas.RegistroPage;
import org.testng.annotations.Test;

public class RegistroConNomApelTelefPassTestWeb extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

        LoginPage loginPage = new LoginPage(driver);
        RegistroPage registroPage = new RegistroPage(driver);
        loginPage.clickOnRegister(hover);
        registroPage.firstname.sendKeys("lolo");
        registroPage.lastName.sendKeys("lolo");
        registroPage.email.sendKeys("lolo@lolo.com");
        registroPage.telefono.sendKeys("lolo");
        registroPage.password.sendKeys("lolo");
        registroPage.confimPassword.clear();
        registroPage.botonContinue.click();

        registroPage.errorMensajePoliticaPrivacidad.isDisplayed();
        registroPage.errorMensajeConfirmPass.isDisplayed();
    }
}
