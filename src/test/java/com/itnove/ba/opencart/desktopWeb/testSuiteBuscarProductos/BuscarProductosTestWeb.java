package com.itnove.ba.opencart.desktopWeb.testSuiteBuscarProductos;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.paginas.BusquedaPage;
import org.testng.annotations.Test;

public class BuscarProductosTestWeb extends BaseTest {

    @Test
    public void testBusquedaConElementos() throws InterruptedException {

        BusquedaPage busquedaPage = new BusquedaPage(driver);
        busquedaPage.textBoxBusqueda.clear();
        busquedaPage.textBoxBusqueda.sendKeys("iphone");
        busquedaPage.botonBusqueda.click();
        busquedaPage.textBusquedaIsPresent(wait);
        busquedaPage.botonAnyadirAlCarroPrimerElemento.click();
        busquedaPage.mensajeSeAnyadeElemntoExitoIsPrensent(wait);

    }
    @Test
    public void testBusquedasinElementos() throws InterruptedException {

    }

}
