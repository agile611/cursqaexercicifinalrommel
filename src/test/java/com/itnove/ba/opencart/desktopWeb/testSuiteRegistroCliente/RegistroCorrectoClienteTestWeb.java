package com.itnove.ba.opencart.desktopWeb.testSuiteRegistroCliente;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.paginas.LoginPage;
import com.itnove.ba.opencart.paginas.RegistroPage;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

public class RegistroCorrectoClienteTestWeb extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

        LoginPage loginPage = new LoginPage(driver);
        RegistroPage registroPage = new RegistroPage(driver);
        loginPage.clickOnRegister(hover);
        registroPage.firstname.sendKeys("lolo");
        registroPage.lastName.sendKeys("lolo");
        registroPage.email.sendKeys("lolo@lololo.com");
        registroPage.telefono.sendKeys("12345678");
        registroPage.password.sendKeys("lolo");
        registroPage.confimPassword.sendKeys("lolo");
        registroPage.politicaPrivacidad.click();

        registroPage.botonContinue.click();
        wait.until(ExpectedConditions.visibilityOf(registroPage.exitoMensaje));
        registroPage.exitoMensaje.isDisplayed();
    }
}
