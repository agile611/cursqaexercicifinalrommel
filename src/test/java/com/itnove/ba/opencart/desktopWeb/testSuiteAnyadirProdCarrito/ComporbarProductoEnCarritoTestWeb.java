package com.itnove.ba.opencart.desktopWeb.testSuiteAnyadirProdCarrito;


import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.paginas.CarritoPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class ComporbarProductoEnCarritoTestWeb extends BaseTest {

    String elementoEnStock = "iPhone";

    @Test
    public void testExisteProductosCarro() throws InterruptedException {


        AnyadirProductosTestWeb anyadirProductosTestWeb= new AnyadirProductosTestWeb();
        CarritoPage carrito = new CarritoPage(driver);

        anyadirProductosTestWeb.testAnyadirProductosCarro(driver,wait,elementoEnStock);
        carrito.botonCarrito.click();
        assertTrue(carrito.existeElemntoLista(elementoEnStock));

    }


}
