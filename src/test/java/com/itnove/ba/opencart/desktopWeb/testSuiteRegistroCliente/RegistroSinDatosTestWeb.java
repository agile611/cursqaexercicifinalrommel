package com.itnove.ba.opencart.desktopWeb.testSuiteRegistroCliente;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.paginas.LoginPage;
import com.itnove.ba.opencart.paginas.RegistroPage;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

public class RegistroSinDatosTestWeb extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

        LoginPage loginPage = new LoginPage(driver);
        RegistroPage registroPage = new RegistroPage(driver);
        loginPage.clickOnRegister(hover);
        registroPage.firstname.clear();
        registroPage.lastName.clear();
        registroPage.email.clear();
        registroPage.telefono.clear();
        registroPage.password.clear();
        registroPage.confimPassword.clear();

        registroPage.botonContinue.click();
        registroPage.errorMensajePoliticaPrivacidad.isDisplayed();
        registroPage.errorMensajeFirstName.isDisplayed();
        registroPage.errorMensajeLastName.isDisplayed();

        registroPage.errorMensajeTelefono.isDisplayed();
        registroPage.errorMensajePass.isDisplayed();

    }
}
