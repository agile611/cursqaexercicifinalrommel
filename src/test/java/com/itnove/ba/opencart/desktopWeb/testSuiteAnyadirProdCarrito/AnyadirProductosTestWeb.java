package com.itnove.ba.opencart.desktopWeb.testSuiteAnyadirProdCarrito;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.paginas.BusquedaPage;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class AnyadirProductosTestWeb extends BaseTest {

    String elementoEnStock= "iphone";


    @Test
    public void testAnyadirProductosCarro() throws InterruptedException {

        BusquedaPage busquedaPage = new BusquedaPage(driver);
        busquedaPage.textBoxBusqueda.clear();
        busquedaPage.textBoxBusqueda.sendKeys(elementoEnStock);
        busquedaPage.botonBusqueda.click();
        busquedaPage.textBusquedaIsPresent(wait);
        busquedaPage.botonAnyadirAlCarroPrimerElemento.click();
    }

    @Test
    public void testAnyadirProductosCarro(RemoteWebDriver driver1, WebDriverWait wait1, String producto) throws InterruptedException {

        BusquedaPage busquedaPage = new BusquedaPage(driver1);
        //busquedaPage.textBoxBusqueda.clear();
        busquedaPage.textBoxBusqueda.sendKeys(producto);
        busquedaPage.botonBusqueda.click();
        busquedaPage.textBusquedaIsPresent(driver1, wait1);
        busquedaPage.botonAnyadirAlCarroPrimerElemento.click();
    }
}
